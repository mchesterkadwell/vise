# Users of VISE

 * [RKD Netherlands Institute of Art History](https://rkd.nl/en/)
 * [Ashmolean Museum](https://www.ashmolean.org/)
 * [Spanish Chapbooks Project @ Cambridge](https://www.cdh.cam.ac.uk/news/spanish-chapbooks)
 * Frank Scholten Project at the [University of Leiden](https://www.universiteitleiden.nl/en)
 * [Sistemas de Informação](https://sistemasfuturo.pt/) for a project by Art Institute at Lisbon University
 * [University of Venice](https://www.unive.it/vedph)

***

Contact [Abhishek Dutta](mailto:adutta@robots.ox.ac.uk) for queries and feedback related to this page.
